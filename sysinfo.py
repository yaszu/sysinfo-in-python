#!/usr/bin/env python3.6

from pathlib import Path
import platform
import re

arch = platform.machine() #Arch
systemname = platform.system() #SytemName
processor = platform.processor() #processor
kernelversion = platform.release() #kernel version
dist = platform.dist() #distro
dist = " ".join(x for x in dist) #distro

with open("/proc/meminfo") as fileOpen: MemFile = fileOpen.read()
fileOpen.closed

memTotal = re.search("MemTotal:.*\n", MemFile)
memFree = re.search("MemFree:.*\n", MemFile)
buffers = re.search("Buffers:.*\n", MemFile)
cached = re.search("Cached:.*\n", MemFile)
slab = re.search("Slab:.*\n", MemFile)

memTotal = int(re.sub("[^0-9]", "", memTotal.group(0)))
memFree = int(re.sub("[^0-9]", "", memFree.group(0)))
buffers = int(re.sub("[^0-9]", "", buffers.group(0)))
cached = int(re.sub("[^0-9]", "", cached.group(0)))
slab = int(re.sub("[^0-9]", "", slab.group(0)))

memUsed = memTotal - memFree - buffers - cached - slab
memTotal = float(memTotal/1024)
memUsed = float(memUsed/1024)

if Path("/usr/include/gnu").exists(): libc = "glibc"
elif not Path("/usr/include/gnu").exists(): libc ="musl"

if Path("/usr/bin/pulseaudio").exists(): audio = "Pulseaudio"
elif not Path("/usr/bin/pulseaudio").exists(): audio = "Alsa"

if Path("/etc/portage/make.conf").exists():
    with open("/etc/portage/make.conf") as fileOpen: confFile = fileOpen.read()
    fileOpen.closed
    cflags = re.search("CFLAGS=.*", confFile)
    cxxflags = re.search("CXXFLAGS=.*", confFile)
    cflags = cflags.group(0)
    cxxflags = cxxflags.group(0)

if not Path("/etc/portage/make.conf").exists():
    cflags = "InstallGentoo"
    cxxflags = "https://wiki.gentoo.org/wiki/Handbook:Parts/Full/Installation"

elif Path("/sbin/openrc").exists(): Init = "OpenRC"
elif Path("/etc/runit").exists(): Init = "Runit"
elif Path("/etc/systemd").exists(): Init = "SystemD"

if Path("/etc/default/grub").exists(): boot = "GRUB"
elif Path("/sbin/lilo").exists(): boot = "Lilo"
elif Path("/boot/extlinux").exists(): boot = "Syslinux(extlinux)"

elif Path("/etc/NetworkManager").exists(): Wireless = "NetworkManager"
if not Path("/etc/NetworkManager").exists(): Wireless = "wpa_supplicant"
if Path("/etc/dhcpcd.conf").exists(): dhcp = "dhcpcd"
if Path("/etc/dhcpcd.conf","/etc/dhcp").exists(): dhcp = "dhcpcd + dhclient"
if Path("/etc.dhcp").exists(): dhcp = "dhclient"


print("\nArch:       {}".format(arch),
      "\nProc:       {}".format(processor),
      "\nKernel:     {} {}".format(systemname,kernelversion),
      "\nDistro:     {}".format(dist),
      "\nLibc:       {}".format(libc),
      "\nMem:        {:.0f}MiB / {:.0f}MiB".format(memUsed,memTotal),
      "\nAudio:      {}".format(audio),
      "\nC:          {}".format(cflags),
      "\nCPP:        {}".format(cxxflags),
      "\nInit:       {}".format(Init),
      "\nBootloader: {}".format(boot),
      "\nWireless:   {} + {}".format(Wireless,dhcp),
      "\n")
